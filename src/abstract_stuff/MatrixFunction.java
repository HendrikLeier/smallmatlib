package abstract_stuff;

import basic_stuff.Matrix;

/**
 * Created by Hendrik on 15.09.2018.
 */
public abstract class MatrixFunction {

    public Matrix compute(Matrix ... matrix) {

        float[] element = new float[matrix.length];

        int[] mat_size = matrix[0].getShape();

        Matrix out = new Matrix(mat_size[1], mat_size[0]);

        for (int y = 0; y < mat_size[1]; y++){
            for (int x = 0; x < mat_size[0]; x++){
                for (int i = 0; i < matrix.length; i++){
                    element[i] = matrix[i].getValue(y, x);

                    float f_out = compute(element);
                    out.setValue(y, x, f_out);
                }
            }
        }

        return out;
    }

    public abstract float compute(float ... in);

}
