package basic_stuff;

import abstract_stuff.MatrixFunction;

/**
 * Created by Hendrik on 14.09.2018.
 */
public class Operations {

    public static Matrix dot(Matrix m1, Matrix m2) {
        int[] dim_m1 = m1.getShape();
        int[] dim_m2 = m2.getShape();


        if(dim_m1[0] == dim_m2[1]) {
            Matrix out = new Matrix(dim_m1[1], dim_m2[0]);

            for(int m1_y = 0; m1_y < dim_m1[1]; m1_y++){
                for(int m2_x = 0; m2_x < dim_m2[0]; m2_x++){
                    for (int m1_x = 0; m1_x < dim_m1[0]; m1_x++){
                        float m1_val = m1.getValue(m1_y, m1_x);
                        float m2_val = m2.getValue(m1_x, m2_x);

                        out.addValue(m1_y, m2_x, m1_val * m2_val);
                    }
                }
            }

            return out;
        } else {
            System.err.println("basic_stuff.Matrix shapes are not matching!");
            return null;
        }
    }

    public static Matrix add(Matrix matrix, float val) {
        int[] dim = matrix.getShape();

        Matrix out = new Matrix(dim[1], dim[0]);

        for (int y = 0; y < dim[1]; y++){
            for (int x = 0; x < dim[0]; x++){
                float m_val = matrix.getValue(y, x);
                out.setValue(y, x, m_val + val);
            }
        }

        return out;
    }

    public static Matrix sub(Matrix matrix, float val) {
        return add(matrix, -val);
    }

    public static Matrix mul(Matrix matrix, float val) {
        int[] dim = matrix.getShape();

        Matrix out = new Matrix(dim[1], dim[0]);

        for (int y = 0; y < dim[1]; y++){
            for (int x = 0; x < dim[0]; x++){
                float m_val = matrix.getValue(y, x);
                out.setValue(y, x, m_val * val);
            }
        }

        return out;
    }

    public static Matrix add(Matrix matrix, Matrix matrix2) {
        int[] dim = matrix.getShape();
        int[] dim2 = matrix2.getShape();

        if(dim[0] == dim2[0] && dim[1] == dim2[1]) {
            Matrix out = new Matrix(dim[1], dim[0]);

            for (int y = 0; y < dim[1]; y++) {
                for (int x = 0; x < dim[0]; x++) {
                    float m_val = matrix.getValue(y, x);
                    float m_val2 = matrix2.getValue(y, x);
                    out.setValue(y, x, m_val + m_val2);
                }
            }

            return out;
        }else return null;
    }

    public static Matrix sub(Matrix matrix, Matrix matrix2) {
        int[] dim = matrix.getShape();
        int[] dim2 = matrix2.getShape();

        if(dim[0] == dim2[0] && dim[1] == dim2[1]) {
            Matrix out = new Matrix(dim[1], dim[0]);

            for (int y = 0; y < dim[1]; y++) {
                for (int x = 0; x < dim[0]; x++) {
                    float m_val = matrix.getValue(y, x);
                    float m_val2 = matrix2.getValue(y, x);
                    out.setValue(y, x, m_val - m_val2);
                }
            }

            return out;
        }else return null;
    }

    public static Matrix mul(Matrix matrix, Matrix matrix2) {
        int[] dim = matrix.getShape();
        int[] dim2 = matrix2.getShape();

        if(dim[0] == dim2[0] && dim[1] == dim2[1]) {
            Matrix out = new Matrix(dim[1], dim[0]);

            for (int y = 0; y < dim[1]; y++) {
                for (int x = 0; x < dim[0]; x++) {
                    float m_val = matrix.getValue(y, x);
                    float m_val2 = matrix2.getValue(y, x);
                    out.setValue(y, x, m_val * m_val2);
                }
            }

            return out;
        }else return null;
    }

    public static Matrix div(Matrix matrix, Matrix matrix2) {
        int[] dim = matrix.getShape();
        int[] dim2 = matrix2.getShape();

        if(dim[0] == dim2[0] && dim[1] == dim2[1]) {
            Matrix out = new Matrix(dim[1], dim[0]);

            for (int y = 0; y < dim[1]; y++) {
                for (int x = 0; x < dim[0]; x++) {
                    float m_val = matrix.getValue(y, x);
                    float m_val2 = matrix2.getValue(y, x);
                    out.setValue(y, x, m_val / m_val2);
                }
            }

            return out;
        }else return null;
    }

    public static Matrix div(Matrix matrix, float val) {
        return mul(matrix, 1.0f / val);
    }

    public static Matrix div(float val, Matrix matrix) {
        int[] dim = matrix.getShape();

        Matrix matrix1 = new Matrix(dim[1], dim[0]);

        matrix1.setVal(val);

        return div(matrix1, matrix);
    }

    public static float mean(Matrix matrix) {

        int[] shape = matrix.getShape();

        float sum = 0;

        for (int y = 0; y < shape[1]; y++){
            for (int x = 0; x < shape[0]; x++){
                sum += matrix.getValue(y, x);
            }
        }

        return sum / (shape[0] * shape[1]);
    }

}
