package basic_stuff;

import java.util.Random;

/**
 * Created by Hendrik on 14.09.2018.
 */
public class Matrix {

    private float[][] data;

    private int[] shape;

    public Matrix(int y, int x){
        this.shape = new int[] {x, y};

        this.data = new float[y][x];
    }

    public Matrix(int y, int x, boolean rand){
        this(y, x);
        if(rand) {
            setRandom(1);
        }
    }

    public void setRandom(long seed) {
        Random random = new Random(seed);
        for (int y = 0; y < shape[1]; y++){
            for (int x = 0; x < shape[0]; x++){
                data[y][x] = random.nextFloat();
            }
        }
    }

    public void setVal(float val) {
        for (int y = 0; y < shape[1]; y++){
            for (int x = 0; x < shape[0]; x++){
                data[y][x] = val;
            }
        }
    }

    public void setInc() {
        int i = 1;

        for (int y = 0; y < shape[1]; y++){
            for (int x = 0; x < shape[0]; x++){
                data[y][x] = i;
                i++;
            }
        }
    }

    public void setValue(int y, int x, float val) {
        data[y][x] = val;
    }

    public void addValue(int y, int x, float val) {
        data[y][x] += val;
    }

    public float getValue(int y, int x) {
        return this.data[y][x];
    }

    public int[] getShape() {
        return shape;
    }

    public void setValue(float[][] val) {
        this.data = val;
    }

    public void setShape(int[] shape) {
        this.shape = shape;
    }

    public Matrix dot(Matrix matrix) {
        return Operations.dot(this, matrix);
    }

    public Matrix mul(Matrix matrix) {
        return Operations.mul(this, matrix);
    }

    public Matrix mul(float val) {
        return Operations.mul(this, val);
    }

    public Matrix div(Matrix matrix) {
        return Operations.div(this, matrix);
    }

    public Matrix div(float val) {
        return Operations.div(this, val);
    }

    public Matrix sub(Matrix matrix){
        return Operations.sub(this, matrix);
    }

    public Matrix sub(float val){
        return Operations.sub(this, val);
    }

    public Matrix add(Matrix matrix){
        return Operations.add(this, matrix);
    }

    public Matrix add(float val){
        return Operations.add(this, val);
    }

    public float mean(){
        return Operations.mean(this);
    }

    public Matrix transpose() {

        Matrix out = new Matrix(this.shape[0], this.shape[1]);

        for (int y = 0; y < shape[1]; y++){
            for (int x = 0; x < shape[0]; x++){
                out.setValue(x, y, this.getValue(y, x));
            }
        }

        return out;
    }

    @Override
    public String toString() {
        String out = "";

        for (int y = 0; y < shape[1]; y++){
            out += "| ";
            for (int x = 0; x < shape[0]; x++){
                out += this.getValue(y,x)+"\t";
            }
            out += "|\n";
        }

        return out;
    }
}
